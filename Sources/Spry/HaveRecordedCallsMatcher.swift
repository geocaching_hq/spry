//
//  HaveRecordedCallsMatcher.swift
//  SpryExample
//
//  Created by Brian Radebaugh on 3/25/18.
//  Copyright © 2018 Brian Radebaugh. All rights reserved.
//

import Nimble

/**
 Nimble matcher used to determine if at least one call has been made.

 - Important: This function respects `resetCalls()`. If calls have been made, then afterward `resetCalls()` is called. It is expected that hasRecordedCalls to be false.
 */
public func haveRecordedCalls<T: Spyable>() -> Matcher<T> {
    return Matcher.define("have recorded calls") { actualExpression, msg in
        guard let spyable = try actualExpression.evaluate() else {
            return MatcherResult(status: .fail, message: msg)
        }

        let success = !spyable._callsDictionary.calls.isEmpty
        let message: ExpectationMessage = .expectedCustomValueTo(
            msg.expectedMessage,
            actual: descriptionOfActual(count: spyable._callsDictionary.calls.count)
        )
        
        return MatcherResult(bool: success, message: message)
    }
}

/**
 Nimble matcher used to determine if at least one call has been made.

 - Important: This function respects `resetCalls()`. If calls have been made, then afterward `resetCalls()` is called. It is expected that hasRecordedCalls to be false.
 */
public func haveRecordedCalls<T: Spyable>() -> Matcher<T.Type> {
    return Matcher.define("have recorded calls") { actualExpression, msg in
        guard let spyable = try actualExpression.evaluate() else {
            return MatcherResult(status: .fail, message: msg)
        }

        let success = !spyable._callsDictionary.calls.isEmpty
        let message: ExpectationMessage = .expectedCustomValueTo(
            msg.expectedMessage,
            actual: descriptionOfActual(count: spyable._callsDictionary.calls.count)
        )

        return MatcherResult(bool: success, message: message)
    }
}

// MARK: - Private Helpers

private func descriptionOfActual(count: Int) -> String {
    let pluralism = count == 1 ? "" : "s"
    return "\(count) call\(pluralism)"
}
