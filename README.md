# Spry

Spry is a framework that allows spying and stubbing in Apple's Swift language. Also included is a Nimble matcher for the spied objects.

Spry was built by Rivukis: https://github.com/Rivukis/Spry
